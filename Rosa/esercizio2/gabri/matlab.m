cp = 3; %componenti principali
cpc = 4; %componenti ptincipali e cluster
c = 3; %numero di cluster

%Componenti principale
for k = 1:1:cp;
    y(1:3000,k) = table2array(ComponentiPrincipali(:,k));
end

%Cluster pi� componeti principali
for i=1:1:cpc;
    clusters(1:3000, i) = table2array(Clusters(:, i));
end

%j � il numero di cluster
%i � il numero di componenti principali
%z � la devianza intracluster per ogni componente principale
for j=1:1:c
    for i=1:1:cp
        z(i, j) = devianza(clusters(clusters(:, cpc) == j,i));
    end
end

%Devianza totale senza tener conto dei cluster
for i=1:1:cp
    dev_colonna(i) = devianza(y(:, i));
end

somma = sum(z);
totale = sum(dev_colonna);
Vpca = 88.818;
Vpersa = (sum(somma)/totale)*Vpca;
Vspiegata = Vpca - Vpersa
