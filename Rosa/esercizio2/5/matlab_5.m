cp = 4; %componenti principali
cpc = cp+1; %componenti ptincipali e cluster
c = 6; %numero di cluster
Vpca = 88.818; %varianza conservata dopo la PCA

tab = xlsread('25'); %mi porto su matlab la tabella di JMP
TT = size(tab); %metto in un vettore le dimensione della tabella
c_t = TT(2); %numero di colonne della tabella
tabella_totale = sortrows(tab,c_t);

%Cluster pi� componeti principali
clusters = tabella_totale(:,(c_t-cpc+1):c_t);

%Componenti principale
y = clusters(:,1:cp);

%j � il numero di cluster
%i � il numero di componenti principali
%z � la devianza intracluster per ogni componente principale
for j=1:1:c
    for i=1:1:cp
        z(i, j) = devianza(clusters(clusters(:, cpc) == j,i));
    end
end

%Devianza totale senza tener conto dei cluster
for i=1:1:cp
    dev_colonna(i) = devianza(y(:, i));
end

somma = sum(z);
totale = sum(dev_colonna);
Vpersa = (sum(somma)/totale)*Vpca;
Vspiegata = Vpca - Vpersa
tot_persa = 100 - Vspiegata

tabella_totale = xlsread('25');

a = WL(tabella_totale, c);
A = a(:,1:(c_t-cpc));
xlswrite('workload',A)
