%la devianza si calcola con la sommatoria di (valori - media dei valori) al
%quadrato 

function [y] = devianza(col)
    sum = 0;
    for i=1:1:size(col)
        dev = (col(i) - mean(col))^2; %(dati - media dei dati)^2
        sum = sum + dev; %effettua una sommatoria
    end
    
    y = sum; %risultato della sommatoria
    
end
