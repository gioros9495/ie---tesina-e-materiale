min_cpu = 1;
max_cpu = 10;
richiesta = 0.990; %la reliability richiesta;
incremento = 2; %incremento reliability
k = -500; %costo in pi� dell'incremento
R_values(1, 1) = 0.80; %reliability un solo processore

for i = 2 : incremento +1
    R_values(1, i) = R_values(1, i-1)+R_values(1, i-1)*0.1;
end

matrice_costi = zeros(max_cpu-min_cpu+1,min_cpu); %matrice 3x8
for j = 1 : incremento +1
    k = k + 500;
    for i = 3 : 1 : 10
        matrice_costi(i,j) = (1000+k)*i;
    end
end

R_sistema = zeros(max_cpu-min_cpu+1,length(R_values)); %matrice 8x3
k=0;
for R = R_values 
    k = k +1;
    for n = min_cpu:max_cpu
        for i = 0:(n-3) %sommatoria per calcolare la reliabiity
            R_sistema(n-min_cpu+1, k) = R_sistema(n-min_cpu+1, k) + nchoosek(n, i)*R^(n-i)*(1-R)^(i); % nchoosek coefficiente binomiale
        end
    end
end

%tutti i valori minori della reliability richiesta li porto a zero
R_sistema2 = R_sistema;
for j = 1 : size(R_sistema2, 2)
    k = k + 500;
    for i = 1 : size(R_sistema2, 1)
        if R_sistema2(i,j)< richiesta
           R_sistema2(i,j)=0;
        end
    end
end

%se nella matrice delle reliebility c'� un valore pari a zero, pongo a zero
%anche il valore nella matrice dei costi nella medesima posizione
matrice_costi2 = matrice_costi;
for j = 1 : size(matrice_costi2, 2)
    for i = 1 : size(matrice_costi2, 1)
        if R_sistema2(i,j)== 0
           matrice_costi2(i,j)=0;
        end
    end
end

%trovo il minimo costo
costo_migliore = min(matrice_costi2(matrice_costi2 > 0))

