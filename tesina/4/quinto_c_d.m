La = 1/5000; %Processimg Unit
Lb = 1/2500; %Remote terminal
Lc = 1/10000; %Bus A
Ld = 1/10000; %Bus B
Le = 1/1000; %INS
Lf = 1/1000; %AHRS
Lg = 1/300; %Doppler

Ra = exp(-La); %Processimg Unit
Rb = exp(-Lb); %Remote terminal
Rc = exp(-Lc); %Bus A
Rd = exp(-Ld); %Bus B
Re = exp(-Le); %INS
Rf = exp(-Lf); %AHRS
Rg = exp(-Lg); %Doppler

R1 = 1 - (1-Ra).^2; 
R2 = 1 - (1-Rb).^2; 
R3 = 1 - (1-Rc).^2; 
R4 = 1 - (1-Rd).^2; 
R5 = 1 - (1-Rf).^3;
R6 = R5*Rg; 
R7 = 1 - (1 - Re)*(1 - R6); 

format long
%reliability sistema
Rsys = R1*R2*R3*R4*R5*R7
%calcolo della c considerando la coverage
c = ((0.9999/ (R2*R3*R4*R5*R7))-Ra)/((1-Ra)*Ra)

%controllo se effettivamente la reliability totale ha 4 nine
R1_cov =Ra+c*(1-Ra)*Ra;
Rsys_cov = R1_cov*R2*R3*R4*R5*R7
