R = 0 : 0.01 :1;
R1 = 1 - (1 - R.^3).^3;
R2 = (1 - (1 - R).^3).^3;
hold on
plot(R1, 'r')
plot(R2, 'b')
legend('parallelo di serie','serie di parallelo')