MIN = 3;
MAX = 10;
R_values = [0.80 0.88 0.96];
R_sistema = zeros(MAX-MIN+1,length(R_values)); %matrice 3x8
k=0;
for R = R_values 
    k = k +1;
    for n = MIN:MAX
        for i = 0:(n-3) %sommatoria per calcolare la reliabiity
            R_sistema(n-MIN+1, k) = R_sistema(n-MIN+1, k) + nchoosek(n, i)*R^(n-i)*(1-R)^(i); % nchoosek coefficiente binomiale
        end
    end
end
