Sample data file lininput_xeon64.

Current date/time: Sun Feb 02 15:58:51 2020

CPU frequency:    3.889 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.006      116.8242 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      118.5747 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.005      133.6478 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      163.1810 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      162.8154 9.834356e-13 3.353768e-02   pass
5000   5008   5      0.635      131.4034 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.592      140.8084 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.435      191.8694 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.438      190.5698 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.423      196.9724 2.206207e-11 3.076378e-02   pass
10000  10000  5      3.164      210.7911 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.253      205.0097 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.335      199.9356 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.621      184.1538 9.111056e-11 3.212651e-02   pass
20000  20016  5      27.312     195.3050 3.888804e-10 3.442444e-02   pass
20000  20016  5      33.447     159.4799 3.888804e-10 3.442444e-02   pass
20000  20016  5      31.678     168.3866 3.888804e-10 3.442444e-02   pass
40000  40000  2      279.889    152.4528 1.578189e-09 3.509946e-02   pass
40000  40000  2      272.795    156.4171 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       139.0086 163.1810
5000   5008   5       170.3247 196.9724
10000  10000  5       199.9726 210.7911
20000  20016  5       174.3905 195.3050
40000  40000  2       154.4350 156.4171

Residual checks PASSED

End of tests

02/02/2020 
16:13
