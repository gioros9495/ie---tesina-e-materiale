Sample data file lininput_xeon64.

Current date/time: Sat Feb 01 18:59:05 2020

CPU frequency:    3.889 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.005      135.7616 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      165.2824 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      149.1361 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.011      62.5477  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.005      146.4832 9.834356e-13 3.353768e-02   pass
5000   5008   5      0.418      199.4091 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.421      197.9877 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.424      196.7971 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.440      189.4653 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.448      186.0900 2.206207e-11 3.076378e-02   pass
10000  10000  5      3.606      184.9302 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.848      173.2881 9.111056e-11 3.212651e-02   pass
10000  10000  5      4.311      154.6754 9.111056e-11 3.212651e-02   pass
10000  10000  5      4.174      159.7490 9.111056e-11 3.212651e-02   pass
20000  20016  5      32.869     162.2860 3.888804e-10 3.442444e-02   pass
20000  20016  5      32.574     163.7541 3.888804e-10 3.442444e-02   pass
20000  20016  5      32.367     164.8001 3.888804e-10 3.442444e-02   pass
40000  40000  2      277.339    153.8547 1.578189e-09 3.509946e-02   pass
40000  40000  2      261.706    163.0452 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       131.8422 165.2824
5000   5008   5       193.9498 199.4091
10000  10000  5       168.1607 184.9302
20000  20016  5       163.6134 164.8001
40000  40000  2       158.4499 163.0452

Residual checks PASSED

End of tests

01/02/2020 
19:13
