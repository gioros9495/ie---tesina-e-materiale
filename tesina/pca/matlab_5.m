cp = 6; %componenti principali
cpc = cp+1; %componenti ptincipali e cluster
c = 7; %numero di cluster
Vpca = 88.818; %varianza conservata dopo la PCA
righe = 300;

tab = xlsread('test_colonne_6_7'); %mi porto su matlab la tabella di JMP
TT = size(tab); %metto in un vettore le dimensione della tabella
c_t = TT(2); %numero di colonne della tabella
tabella_totale = sortrows(tab,c_t);

%devianza totale calcolata rispetto al centroide della pca (zero quindi 
%omesso nel calcolo
devianzaTotale=0;
for i=1:3000
    devianzaTotale =devianzaTotale + norm(pca(i, 1:6)).^2;
end


%Cluster pi� componeti principali
clusters = tabella_totale(:,(c_t-cpc+1):c_t);

%clusters
Nclusters = clusters(:, cpc);

%Componenti principale
y = clusters(:,1:cp);

%centroidi
for i = 1:c
    centroidi(i,1:cp)=mean(clusters( : , cpc )==i , 1 : cp );
end

%intra-cluster
devianzaCluster=zeros ( c , 1 ) ;
for i =1:c
temp = clusters ( clusters( : , cpc )==i , 1 : cp ); %seleziona i singoli cluster
    for j =1: size ( temp , 1 ) %size = quanti valori ci sono nel cluster
        devianzaCluster ( i )= devianzaCluster ( i ) + (norm ( temp ( j , : )- centroidi( i , : ) ) .^2) ;
    end
end
devianzaintra=sum ( devianzaCluster );



Vspiegata = Vpca - Vpca*(devianzaintra/devianzaTotale)
tot_persa = 100 - Vspiegata

tabella_totale = xlsread('test_colonne_6_7');

a = WL(tabella_totale, c);
A = a(:,1:(c_t-cpc));
xlswrite('workload',A)
