cp = 4; %componenti principali
cpc = cp+1; %componenti principali e cluster
c = 8; %numero di cluster
Vpca = 88.818; %varianza conservata dopo la PCA
tabella_xlsx = 'pca_clustering_4_8';

tab = xlsread(tabella_xlsx); %mi porto su matlab la tabella di JMP
TT = size(tab); %metto in un vettore le dimensione della tabella
c_t = TT(2); %numero di colonne della tabella
tabella_totale = sortrows(tab,c_t);

%Cluster pi� componeti principali
clusters_CP = tabella_totale(:,(c_t-cpc+1):c_t);

%Componenti principale
componenti_principali = clusters_CP(:,1:cp);

%Cluster
cluster = tabella_totale(:, c_t);

%Numero di righe
righe = size(cluster,1);

%media complessiva
for i = 1:cp
    centroide_pca(i) = mean(componenti_principali(:,i)); %sono numeri molto vicini allo zero
end

%devianza totale calcolata rispetto al centroide della pca (zero quindi
%omesso nel calcolo)
devianzaTotale=0;
for i = 1:righe
    devianzaTotale = devianzaTotale + norm(componenti_principali(i, :)).^2;
end

%calcolo dei centroidi (trova il centroide di ciascun cluster per ogni componente)
%il centroide � la mediana nel cluster 
for i = 1:c
   centroidi(i, 1:cp)=mean(clusters_CP(cluster==i,1:cp)); 
end

%calcolo devianze intra-cluster
devianzaCluster = zeros(c,1);
for i=1:c
    temp = clusters_CP(cluster==i,1:cp); %prendo i valori all'interno dei singoli cluster
    for j=1:size(temp,1)
        devianzaCluster(i) = devianzaCluster(i) + norm(temp(j,:)-centroidi(i,:)).^2;
    end
end
devianzaIntra=sum(devianzaCluster);

Vtot = Vpca - Vpca * (devianzaIntra/devianzaTotale)
Vpersa = 100 - Vtot

tabella_totale = xlsread(tabella_xlsx);

a = WL(tabella_totale, c);
A = a(:,1:(c_t-cpc));
xlswrite('workload',A)
