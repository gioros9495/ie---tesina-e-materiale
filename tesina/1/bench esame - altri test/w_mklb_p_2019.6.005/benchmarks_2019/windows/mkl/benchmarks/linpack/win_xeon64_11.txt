Sample data file lininput_xeon64.

Current date/time: Fri Jan 24 15:23:34 2020

CPU frequency:    2.892 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.007      98.8158  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      114.1478 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      121.3067 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.005      147.1473 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.005      138.8053 9.834356e-13 3.353768e-02   pass
5000   5008   5      0.428      194.9926 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.436      191.0387 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.495      168.4393 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.746      111.7357 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.628      132.8622 2.206207e-11 3.076378e-02   pass
10000  10000  5      8.975      74.3047  9.297604e-11 3.278429e-02   pass
10000  10000  5      5.244      127.1746 9.111056e-11 3.212651e-02   pass
10000  10000  5      5.213      127.9160 9.111056e-11 3.212651e-02   pass
10000  10000  5      5.205      128.1086 9.111056e-11 3.212651e-02   pass
20000  20016  5      40.301     132.3569 3.888804e-10 3.442444e-02   pass
20000  20016  5      40.304     132.3482 3.888804e-10 3.442444e-02   pass
20000  20016  5      40.248     132.5308 3.888804e-10 3.442444e-02   pass
40000  40000  2      417.090    102.3038 1.578189e-09 3.509946e-02   pass
40000  40000  2      282.151    151.2304 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       124.0446 147.1473
5000   5008   5       159.8137 194.9926
10000  10000  5       114.3760 128.1086
20000  20016  5       132.4120 132.5308
40000  40000  2       126.7671 151.2304

Residual checks PASSED

End of tests

24/01/2020 
15:42
