Sample data file lininput_xeon64.

Current date/time: Sun Feb 02 16:14:47 2020

CPU frequency:    3.887 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.007      98.0694  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      107.8634 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.007      92.9155  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.007      95.9763  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      105.8704 9.834356e-13 3.353768e-02   pass
5000   5008   5      0.418      199.5822 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.421      197.9283 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.414      201.5462 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.423      196.9618 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.482      173.0512 2.206207e-11 3.076378e-02   pass
10000  10000  5      3.226      206.6938 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.457      192.8848 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.334      200.0330 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.309      201.5423 9.111056e-11 3.212651e-02   pass
20000  20016  5      32.239     165.4584 3.888804e-10 3.442444e-02   pass
20000  20016  5      32.766     162.7971 3.888804e-10 3.442444e-02   pass
20000  20016  5      31.918     167.1184 3.888804e-10 3.442444e-02   pass
40000  40000  2      257.644    165.6153 1.578189e-09 3.509946e-02   pass
40000  40000  2      256.639    166.2639 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       100.1390 107.8634
5000   5008   5       193.8139 201.5462
10000  10000  5       200.2885 206.6938
20000  20016  5       165.1246 167.1184
40000  40000  2       165.9396 166.2639

Residual checks PASSED

End of tests

02/02/2020 
16:29
