Sample data file lininput_xeon64.

Current date/time: Sat Feb 01 16:44:38 2020

CPU frequency:    3.887 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.005      128.3454 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      154.4087 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      150.5328 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      161.9479 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      167.1124 9.834356e-13 3.353768e-02   pass
5000   5008   5      0.410      203.5921 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.421      197.9512 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.441      188.9104 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.445      187.2345 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.460      181.4024 2.206207e-11 3.076378e-02   pass
10000  10000  5      3.305      201.7516 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.422      194.8806 9.111056e-11 3.212651e-02   pass
10000  10000  5      4.161      160.2497 9.111056e-11 3.212651e-02   pass
10000  10000  5      4.300      155.0738 9.111056e-11 3.212651e-02   pass
20000  20016  5      33.474     159.3494 3.888804e-10 3.442444e-02   pass
20000  20016  5      32.358     164.8480 3.888804e-10 3.442444e-02   pass
20000  20016  5      32.366     164.8046 3.888804e-10 3.442444e-02   pass
40000  40000  2      260.684    163.6840 1.578189e-09 3.509946e-02   pass
40000  40000  2      270.556    157.7120 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       152.4694 167.1124
5000   5008   5       191.8181 203.5921
10000  10000  5       177.9889 201.7516
20000  20016  5       163.0007 164.8480
40000  40000  2       160.6980 163.6840

Residual checks PASSED

End of tests

01/02/2020 
16:59
