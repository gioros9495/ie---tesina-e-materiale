Sample data file lininput_xeon64.

Current date/time: Sun Feb 02 15:04:12 2020

CPU frequency:    3.582 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.006      106.2354 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      112.1660 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.008      88.1320  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.014      47.2326  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.185      3.6162   9.834356e-13 3.353768e-02   pass
5000   5008   5      0.900      92.6298  2.206207e-11 3.076378e-02   pass
5000   5008   5      0.785      106.1603 2.367906e-11 3.301854e-02   pass
5000   5008   5      1.228      67.9166  2.464151e-11 3.436060e-02   pass
5000   5008   5      0.593      140.5094 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.634      131.5412 2.206207e-11 3.076378e-02   pass
10000  10000  5      7.147      93.3012  9.111056e-11 3.212651e-02   pass
10000  10000  5      5.589      119.3077 9.111056e-11 3.212651e-02   pass
10000  10000  5      5.267      126.6092 9.111056e-11 3.212651e-02   pass
10000  10000  5      4.928      135.3187 9.111056e-11 3.212651e-02   pass
20000  20016  5      47.275     112.8316 3.888804e-10 3.442444e-02   pass
20000  20016  5      37.198     143.3988 3.888804e-10 3.442444e-02   pass
20000  20016  5      35.321     151.0180 3.888804e-10 3.442444e-02   pass
40000  40000  2      295.181    144.5550 1.578189e-09 3.509946e-02   pass
40000  40000  2      262.251    162.7064 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       71.4764  112.1660
5000   5008   5       107.7515 140.5094
10000  10000  5       118.6342 135.3187
20000  20016  5       135.7495 151.0180
40000  40000  2       153.6307 162.7064

Residual checks PASSED

End of tests

02/02/2020 
15:22
