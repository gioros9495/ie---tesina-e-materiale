Sample data file lininput_xeon64.

Current date/time: Sat Feb 01 12:10:22 2020

CPU frequency:    3.889 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.009      72.2040  9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      114.1517 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      108.9069 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.006      117.0287 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.005      125.3899 9.834356e-13 3.353768e-02   pass
5000   5008   5      0.503      165.7951 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.460      181.2980 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.480      173.5979 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.480      173.8162 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.442      188.6782 2.206207e-11 3.076378e-02   pass
10000  10000  5      3.314      201.2054 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.571      186.7302 9.111056e-11 3.212651e-02   pass
10000  10000  5      4.013      166.1636 9.111056e-11 3.212651e-02   pass
10000  10000  5      4.392      151.8262 9.111056e-11 3.212651e-02   pass
20000  20016  5      34.111     156.3753 3.888804e-10 3.442444e-02   pass
20000  20016  5      33.269     160.3317 3.888804e-10 3.442444e-02   pass
20000  20016  5      33.507     159.1967 3.888804e-10 3.442444e-02   pass
40000  40000  2      266.924    159.8577 1.578189e-09 3.509946e-02   pass
40000  40000  2      313.550    136.0864 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       107.5362 125.3899
5000   5008   5       176.6371 188.6782
10000  10000  5       176.4814 201.2054
20000  20016  5       158.6346 160.3317
40000  40000  2       147.9721 159.8577

Residual checks PASSED

End of tests

01/02/2020 
12:26
