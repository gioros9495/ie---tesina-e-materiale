Sample data file lininput_xeon64.

Current date/time: Fri Jan 24 15:46:08 2020

CPU frequency:    3.889 GHz
Number of CPUs: 1
Number of cores: 6
Number of threads: 6

Parameters are set to:

Number of tests: 5

Number of equations to solve (problem size) : 1000  5000  10000 20000 40000
Leading dimension of array                  : 1000  5008  10000 20016 40000
Number of trials to run                     : 5     5     4     3     2    
Data alignment value (in Kbytes)            : 5     5     5     5     2    
Maximum memory requested that can be used=12800802048, at the size=40000

=================== Timing linear equation system solver ===================

Size   LDA    Align. Time(s)    GFlops   Residual     Residual(norm) Check
1000   1000   5      0.005      134.8173 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      155.7139 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.005      146.9532 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      154.3196 9.834356e-13 3.353768e-02   pass
1000   1000   5      0.004      173.8693 9.834356e-13 3.353768e-02   pass
5000   5008   5      0.480      173.8541 2.386777e-11 3.328168e-02   pass
5000   5008   5      0.440      189.4003 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.426      195.9509 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.428      194.9285 2.206207e-11 3.076378e-02   pass
5000   5008   5      0.474      176.0694 2.206207e-11 3.076378e-02   pass
10000  10000  5      3.401      196.1010 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.469      192.2090 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.342      199.5161 9.111056e-11 3.212651e-02   pass
10000  10000  5      3.578      186.3901 9.111056e-11 3.212651e-02   pass
20000  20016  5      27.408     194.6214 3.888804e-10 3.442444e-02   pass
20000  20016  5      31.941     166.9987 3.888804e-10 3.442444e-02   pass
20000  20016  5      31.381     169.9779 3.888804e-10 3.442444e-02   pass
40000  40000  2      271.429    157.2043 1.578189e-09 3.509946e-02   pass
40000  40000  2      264.187    161.5141 1.578189e-09 3.509946e-02   pass

Performance Summary (GFlops)

Size   LDA    Align.  Average  Maximal
1000   1000   5       153.1347 173.8693
5000   5008   5       186.0406 195.9509
10000  10000  5       193.5540 199.5161
20000  20016  5       177.1993 194.6214
40000  40000  2       159.3592 161.5141

Residual checks PASSED

End of tests

24/01/2020 
16:00
