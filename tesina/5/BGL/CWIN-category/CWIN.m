J18_U11  = load('tupleCount-J18-U11-BGLErrorLog.txt'); 
figure(1)
plot(J18_U11 (:,1), J18_U11 (:,2),'-*b')
ylabel('count');
xlabel('CWIN');
legend('J18-U11');
 
J18_U01  = load('tupleCount-J18-U01-BGLErrorLog.txt'); 
figure(2)
plot(J18_U01 (:,1), J18_U01 (:,2),'-*g')
ylabel('count');
xlabel('CWIN');
legend('J18-U01');

J14_U01  = load('tupleCount-J14-U01-BGLErrorLog.txt'); 
figure(3)
plot(J14_U01 (:,1), J14_U01 (:,2),'-*m')
ylabel('count');
xlabel('CWIN');
legend('J14-U01');
 
J12_U01  = load('tupleCount-J12-U01-BGLErrorLog.txt'); 
figure(4)
plot(J12_U01 (:,1), J12_U01 (:,2),'-*c')
ylabel('count');
xlabel('CWIN');
legend('J12-U01');

J07_U01  = load('tupleCount-J07-U01-BGLErrorLog.txt'); 
figure(5)
plot(J07_U01 (:,1), J07_U01 (:,2),'-*r')
ylabel('count');
xlabel('CWIN');
legend('J07-U01');

figure(6)
plot(J18_U11 (:,1), J18_U11 (:,2),'b', J18_U01 (:,1), J18_U01 (:,2),'g', J14_U01 (:,1), J14_U01 (:,2),'m', J12_U01 (:,1), J12_U01 (:,2),'c', J07_U01 (:,1), J07_U01 (:,2),'r')
ylabel('count');
xlabel('CWIN');
legend('J18-U11','J18-U01','J14-U01','J12-U01','J07-U01');





