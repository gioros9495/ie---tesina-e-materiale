R71_M0_N4 = load('tupleCount-R71-M0-N4-BGLErrorLog.txt');
figure(1)
plot(R71_M0_N4(:,1), R71_M0_N4(:,2),'-*b')
ylabel('count');
xlabel('CWIN');
legend('R71-M0-N4');

R12_M0_N0 = load('tupleCount-R12-M0-N0-BGLErrorLog.txt');
figure(2)
plot(R12_M0_N0(:,1), R12_M0_N0(:,2),'-*g')
ylabel('count');
xlabel('CWIN');
legend('R12-M0-N0');

R63_M0_N2 = load('tupleCount-R63-M0-N2-BGLErrorLog.txt'); 
figure(3)
plot(R63_M0_N2(:,1), R63_M0_N2(:,2),'-*m')
ylabel('count');
xlabel('CWIN');
legend('R63-M0-N2');

R03_M1_NF = load('tupleCount-R03-M1-NF-BGLErrorLog.txt');
figure(4)
plot(R03_M1_NF(:,1), R03_M1_NF(:,2),'-*c')
ylabel('count');
xlabel('CWIN');
legend('R03-M1-NF');

R63_M0_N0 = load('tupleCount-R63-M0-N0-BGLErrorLog.txt');
figure(5)
plot(R63_M0_N0(:,1), R63_M0_N0(:,2),'-*r')
ylabel('count');
xlabel('CWIN');
legend('R63-M0-N0');

figure(6)
plot(R71_M0_N4 (:,1), R71_M0_N4 (:,2),'b', R12_M0_N0  (:,1), R12_M0_N0  (:,2),'g', R63_M0_N2  (:,1), R63_M0_N2  (:,2),'m', R03_M1_NF  (:,1), R03_M1_NF  (:,2),'c', R63_M0_N0  (:,1), R63_M0_N0  (:,2),'r')
ylabel('count');
xlabel('CWIN');
legend('R71-M0-N4','R12-M0-N0','R63-M0-N2','R03-M1-NF','R63-M0-N0');