master = load('tg-master-tentative-Cwin.txt');
figure(1)
plot(master(:,1), master(:,2),'-*b')
ylabel('count');
xlabel('CWIN');
legend('tg-c572');

c648 = load('tg-c648-tentative-Cwin.txt'); 
figure(2)
plot(c648(:,1), c648(:,2),'-*g')
ylabel('count');
xlabel('CWIN');
legend('tg-c238');

c572 = load('tg-c572-tentative-Cwin.txt'); 
figure(3)
plot(c572(:,1), c572(:,2),'-*m')
ylabel('count');
xlabel('CWIN');
legend('tg-master');

c242 = load('tg-c242-tentative-Cwin.txt'); 
figure(4)
plot(c242(:,1), c242(:,2),'-*c')
ylabel('count');
xlabel('CWIN');
legend('tg-c242');

c238 = load('tg-c238-tentative-Cwin.txt'); 
figure(5)
plot(c238(:,1), c238(:,2),'-*r')
ylabel('count');
xlabel('CWIN');
legend('tg-c648');

figure(6)
plot(c572(:,1), c572(:,2),'-*b', c238(:,1), c238(:,2),'-*g', master(:,1), master(:,2),'-*m', c242(:,1), c242(:,2),'-*c', c648(:,1), c648(:,2),'-*r')
ylabel('count');
xlabel('CWIN');
legend('tg-c572','tg-c238','tg-master','tg-c242','tg-c648');