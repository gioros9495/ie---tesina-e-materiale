dev = load('DEV-tentative-Cwin.txt'); 
figure(1)
plot(dev(:,1), dev(:,2),'-*b')
ylabel('count');
xlabel('CWIN');
legend('DEV');
 
pro = load('PRO-tentative-Cwin.txt'); 
figure(2)
plot(pro(:,1), pro(:,2),'-*g')
ylabel('count');
xlabel('CWIN');
legend('PRO');

mem = load('MEM-tentative-Cwin.txt'); 
figure(3)
plot(mem(:,1), mem(:,2),'-*m')
ylabel('count');
xlabel('CWIN');
legend('MEM');
 
io = load('I-O-tentative-Cwin.txt'); 
figure(4)
plot(io(:,1), io(:,2),'-*c')
ylabel('count');
xlabel('CWIN');
legend('I-O');

net = load('NET-tentative-Cwin.txt'); 
figure(5)
plot(net(:,1), net(:,2),'-*r')
ylabel('count');
xlabel('CWIN');
legend('NET');

figure(6)
plot(dev(:,1), dev(:,2),'-*b', pro(:,1), pro(:,2),'-*g', mem(:,1), mem(:,2),'-*m', io(:,1), io(:,2),'-*c', net(:,1), net(:,2),'-*r')
ylabel('count');
xlabel('CWIN');
legend('DEV','PRO','MEM','I-O','NET');





