load interarrivals.txt;
[p,t] = cdfcalc(interarrivals);
ttf = p(2:size(p,1));
rel = 1 - ttf;
plot (t, ttf, '-*b', t, rel, '-*r')
xlabel ('time[s]');
ylabel ('p');
legend('empirical TTF', 'empirical rel')
cftool(t,rel);
