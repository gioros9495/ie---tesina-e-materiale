load interarrivals.txt;
[p,t] = cdfcalc(interarrivals);
ttf = p(2:size(p,1));
rel = 1 - ttf;
plot (t, ttf, '-*b', t, rel, '-*r')
xlabel ('time[s]');
ylabel ('p');
legend('empirical TTF', 'empirical rel')
cftool(t,rel);

plot(t, thRel1(t), '-', t, rel, '-+r');
legend('empirical model', 'empirical rel');
xlabel ('time[s]');
ylabel ('p');

plot(t, thRel1(t), '-', t, weibull(t), 'k-', t, rel, '-+r');
legend('empirical model', 'weibull model', 'empirical rel');
xlabel ('time[s]');
ylabel ('p');

[H1, P1, K1] = kstest2(rel, thRel1(t));
[H2, P2, K2] = kstest2(rel, thRel1(t), 'alpha', 0.1);
[H3, P3, K3] = kstest2(rel, weibull(t));


